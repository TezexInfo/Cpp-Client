/**
 * TezosAPI
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



#include "Endorsement.h"

namespace info {
namespace tezex {
namespace client {
namespace model {

Endorsement::Endorsement()
{
    m_Hash = U("");
    m_HashIsSet = false;
    m_Source = U("");
    m_SourceIsSet = false;
    m_Level = nullptr;
    m_LevelIsSet = false;
    m_Block = U("");
    m_BlockIsSet = false;
    m_Endorsed_block = U("");
    m_Endorsed_blockIsSet = false;
    m_Slot = nullptr;
    m_SlotIsSet = false;
    m_Time = utility::datetime();
    m_TimeIsSet = false;
    
}

Endorsement::~Endorsement()
{
}

void Endorsement::validate() 
{
    // TODO: implement validation
}

web::json::value Endorsement::toJson() const
{
    web::json::value val = web::json::value::object();
     
	if(m_HashIsSet)
    {
        val[U("hash")] = ModelBase::toJson(m_Hash);
    }
    if(m_SourceIsSet)
    {
        val[U("source")] = ModelBase::toJson(m_Source);
    }
    if(m_LevelIsSet)
    {
        val[U("level")] = ModelBase::toJson(m_Level);
    }
    if(m_BlockIsSet)
    {
        val[U("block")] = ModelBase::toJson(m_Block);
    }
    if(m_Endorsed_blockIsSet)
    {
        val[U("endorsed_block")] = ModelBase::toJson(m_Endorsed_block);
    }
    if(m_SlotIsSet)
    {
        val[U("slot")] = ModelBase::toJson(m_Slot);
    }
    if(m_TimeIsSet)
    {
        val[U("time")] = ModelBase::toJson(m_Time);
    }
    

    return val;
}

void Endorsement::fromJson(web::json::value& val)
{
    if(val.has_field(U("hash")))
    {
        setHash(ModelBase::stringFromJson(val[U("hash")]));
                
    }
    if(val.has_field(U("source")))
    {
        setSource(ModelBase::stringFromJson(val[U("source")]));
                
    }
    if(val.has_field(U("level")))
    {
        setLevel(ModelBase::int32_tFromJson(val[U("level")]));
    }
    if(val.has_field(U("block")))
    {
        setBlock(ModelBase::stringFromJson(val[U("block")]));
                
    }
    if(val.has_field(U("endorsed_block")))
    {
        setEndorsedBlock(ModelBase::stringFromJson(val[U("endorsed_block")]));
                
    }
    if(val.has_field(U("slot")))
    {
        setSlot(ModelBase::int32_tFromJson(val[U("slot")]));
    }
    if(val.has_field(U("time")))
    {
        setTime(ModelBase::dateFromJson(val[U("time")]));
                
    }
    
}

void Endorsement::toMultipart(std::shared_ptr<MultipartFormData> multipart, const utility::string_t& prefix) const
{
    utility::string_t namePrefix = prefix;
	if(namePrefix.size() > 0 && namePrefix[namePrefix.size() - 1] != U('.'))
	{
		namePrefix += U(".");
	}

	if(m_HashIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + U("hash"), m_Hash));
                
    }
    if(m_SourceIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + U("source"), m_Source));
                
    }
    if(m_LevelIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + U("level"), m_Level));
    }
    if(m_BlockIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + U("block"), m_Block));
                
    }
    if(m_Endorsed_blockIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + U("endorsed_block"), m_Endorsed_block));
                
    }
    if(m_SlotIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + U("slot"), m_Slot));
    }
    if(m_TimeIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + U("time"), m_Time));
                
    }
    
}

void Endorsement::fromMultiPart(std::shared_ptr<MultipartFormData> multipart, const utility::string_t& prefix)
{
    utility::string_t namePrefix = prefix;
	if(namePrefix.size() > 0 && namePrefix[namePrefix.size() - 1] != U('.'))
	{
		namePrefix += U(".");
	}

    if(multipart->hasContent(U("hash")))
    {
        setHash(ModelBase::stringFromHttpContent(multipart->getContent(U("hash"))));
                
    }
    if(multipart->hasContent(U("source")))
    {
        setSource(ModelBase::stringFromHttpContent(multipart->getContent(U("source"))));
                
    }
    if(multipart->hasContent(U("level")))
    {
        setLevel(ModelBase::int32_tFromHttpContent(multipart->getContent(U("level"))));
    }
    if(multipart->hasContent(U("block")))
    {
        setBlock(ModelBase::stringFromHttpContent(multipart->getContent(U("block"))));
                
    }
    if(multipart->hasContent(U("endorsed_block")))
    {
        setEndorsedBlock(ModelBase::stringFromHttpContent(multipart->getContent(U("endorsed_block"))));
                
    }
    if(multipart->hasContent(U("slot")))
    {
        setSlot(ModelBase::int32_tFromHttpContent(multipart->getContent(U("slot"))));
    }
    if(multipart->hasContent(U("time")))
    {
        setTime(ModelBase::dateFromHttpContent(multipart->getContent(U("time"))));
                
    }
    
}
    
   
utility::string_t Endorsement::getHash() const
{
	return m_Hash;
}
void Endorsement::setHash(utility::string_t value)
{
	m_Hash = value;
    m_HashIsSet = true;
}
bool Endorsement::hashIsSet() const
{
    return m_HashIsSet;
}
void Endorsement::unsetHash() 
{
    m_HashIsSet = false;
}
utility::string_t Endorsement::getSource() const
{
	return m_Source;
}
void Endorsement::setSource(utility::string_t value)
{
	m_Source = value;
    m_SourceIsSet = true;
}
bool Endorsement::sourceIsSet() const
{
    return m_SourceIsSet;
}
void Endorsement::unsetSource() 
{
    m_SourceIsSet = false;
}
int32_t Endorsement::getLevel() const
{
	return m_Level;
}
void Endorsement::setLevel(int32_t value)
{
	m_Level = value;
    m_LevelIsSet = true;
}
bool Endorsement::levelIsSet() const
{
    return m_LevelIsSet;
}
void Endorsement::unsetLevel() 
{
    m_LevelIsSet = false;
}
utility::string_t Endorsement::getBlock() const
{
	return m_Block;
}
void Endorsement::setBlock(utility::string_t value)
{
	m_Block = value;
    m_BlockIsSet = true;
}
bool Endorsement::blockIsSet() const
{
    return m_BlockIsSet;
}
void Endorsement::unsetBlock() 
{
    m_BlockIsSet = false;
}
utility::string_t Endorsement::getEndorsedBlock() const
{
	return m_Endorsed_block;
}
void Endorsement::setEndorsedBlock(utility::string_t value)
{
	m_Endorsed_block = value;
    m_Endorsed_blockIsSet = true;
}
bool Endorsement::endorsed_blockIsSet() const
{
    return m_Endorsed_blockIsSet;
}
void Endorsement::unsetEndorsed_block() 
{
    m_Endorsed_blockIsSet = false;
}
int32_t Endorsement::getSlot() const
{
	return m_Slot;
}
void Endorsement::setSlot(int32_t value)
{
	m_Slot = value;
    m_SlotIsSet = true;
}
bool Endorsement::slotIsSet() const
{
    return m_SlotIsSet;
}
void Endorsement::unsetSlot() 
{
    m_SlotIsSet = false;
}
utility::datetime Endorsement::getTime() const
{
	return m_Time;
}
void Endorsement::setTime(utility::datetime value)
{
	m_Time = value;
    m_TimeIsSet = true;
}
bool Endorsement::timeIsSet() const
{
    return m_TimeIsSet;
}
void Endorsement::unsetTime() 
{
    m_TimeIsSet = false;
}

}
}
}
}

