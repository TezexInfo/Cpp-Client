/**
 * TezosAPI
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



#include "Origination.h"

namespace info {
namespace tezex {
namespace client {
namespace model {

Origination::Origination()
{
    m_Hash = U("");
    m_HashIsSet = false;
    m_Branch = U("");
    m_BranchIsSet = false;
    m_Source = U("");
    m_SourceIsSet = false;
    m_Public_key = U("");
    m_Public_keyIsSet = false;
    m_Fee = nullptr;
    m_FeeIsSet = false;
    m_Counter = nullptr;
    m_CounterIsSet = false;
    m_OperationsIsSet = false;
    m_Level = nullptr;
    m_LevelIsSet = false;
    m_Block_hash = U("");
    m_Block_hashIsSet = false;
    m_Time = utility::datetime();
    m_TimeIsSet = false;
    
}

Origination::~Origination()
{
}

void Origination::validate() 
{
    // TODO: implement validation
}

web::json::value Origination::toJson() const
{
    web::json::value val = web::json::value::object();
     
	if(m_HashIsSet)
    {
        val[U("hash")] = ModelBase::toJson(m_Hash);
    }
    if(m_BranchIsSet)
    {
        val[U("branch")] = ModelBase::toJson(m_Branch);
    }
    if(m_SourceIsSet)
    {
        val[U("source")] = ModelBase::toJson(m_Source);
    }
    if(m_Public_keyIsSet)
    {
        val[U("public_key")] = ModelBase::toJson(m_Public_key);
    }
    if(m_FeeIsSet)
    {
        val[U("fee")] = ModelBase::toJson(m_Fee);
    }
    if(m_CounterIsSet)
    {
        val[U("counter")] = ModelBase::toJson(m_Counter);
    }
    {
        std::vector<web::json::value> jsonArray;
        for( auto& item : m_Operations )
        {
            jsonArray.push_back(ModelBase::toJson(item));
        }
        
        if(jsonArray.size() > 0) 
        {
            val[U("operations")] = web::json::value::array(jsonArray);
        }
    }
    if(m_LevelIsSet)
    {
        val[U("level")] = ModelBase::toJson(m_Level);
    }
    if(m_Block_hashIsSet)
    {
        val[U("block_hash")] = ModelBase::toJson(m_Block_hash);
    }
    if(m_TimeIsSet)
    {
        val[U("time")] = ModelBase::toJson(m_Time);
    }
    

    return val;
}

void Origination::fromJson(web::json::value& val)
{
    if(val.has_field(U("hash")))
    {
        setHash(ModelBase::stringFromJson(val[U("hash")]));
                
    }
    if(val.has_field(U("branch")))
    {
        setBranch(ModelBase::stringFromJson(val[U("branch")]));
                
    }
    if(val.has_field(U("source")))
    {
        setSource(ModelBase::stringFromJson(val[U("source")]));
                
    }
    if(val.has_field(U("public_key")))
    {
        setPublicKey(ModelBase::stringFromJson(val[U("public_key")]));
                
    }
    if(val.has_field(U("fee")))
    {
        setFee(ModelBase::int32_tFromJson(val[U("fee")]));
    }
    if(val.has_field(U("counter")))
    {
        setCounter(ModelBase::int32_tFromJson(val[U("counter")]));
    }
    {
        m_Operations.clear();
        std::vector<web::json::value> jsonArray;
        if(val.has_field(U("operations")))
        {
        for( auto& item : val[U("operations")].as_array() )
        {
            
            if(item.is_null()) 
            {
                m_Operations.push_back( std::shared_ptr<OriginationOperation>(nullptr) );
            }
            else
            {
                std::shared_ptr<OriginationOperation> newItem(new OriginationOperation());
                newItem->fromJson(item);
                m_Operations.push_back( newItem );
            }
            
        }
        }
    }
    if(val.has_field(U("level")))
    {
        setLevel(ModelBase::int32_tFromJson(val[U("level")]));
    }
    if(val.has_field(U("block_hash")))
    {
        setBlockHash(ModelBase::stringFromJson(val[U("block_hash")]));
                
    }
    if(val.has_field(U("time")))
    {
        setTime(ModelBase::dateFromJson(val[U("time")]));
                
    }
    
}

void Origination::toMultipart(std::shared_ptr<MultipartFormData> multipart, const utility::string_t& prefix) const
{
    utility::string_t namePrefix = prefix;
	if(namePrefix.size() > 0 && namePrefix[namePrefix.size() - 1] != U('.'))
	{
		namePrefix += U(".");
	}

	if(m_HashIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + U("hash"), m_Hash));
                
    }
    if(m_BranchIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + U("branch"), m_Branch));
                
    }
    if(m_SourceIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + U("source"), m_Source));
                
    }
    if(m_Public_keyIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + U("public_key"), m_Public_key));
                
    }
    if(m_FeeIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + U("fee"), m_Fee));
    }
    if(m_CounterIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + U("counter"), m_Counter));
    }
    {
        std::vector<web::json::value> jsonArray;
        for( auto& item : m_Operations )
        {
            jsonArray.push_back(ModelBase::toJson(item));
        }
        
        if(jsonArray.size() > 0) 
        {
            multipart->add(ModelBase::toHttpContent(namePrefix + U("operations"), web::json::value::array(jsonArray), U("application/json")));
        }
    }
    if(m_LevelIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + U("level"), m_Level));
    }
    if(m_Block_hashIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + U("block_hash"), m_Block_hash));
                
    }
    if(m_TimeIsSet)
    {
        multipart->add(ModelBase::toHttpContent(namePrefix + U("time"), m_Time));
                
    }
    
}

void Origination::fromMultiPart(std::shared_ptr<MultipartFormData> multipart, const utility::string_t& prefix)
{
    utility::string_t namePrefix = prefix;
	if(namePrefix.size() > 0 && namePrefix[namePrefix.size() - 1] != U('.'))
	{
		namePrefix += U(".");
	}

    if(multipart->hasContent(U("hash")))
    {
        setHash(ModelBase::stringFromHttpContent(multipart->getContent(U("hash"))));
                
    }
    if(multipart->hasContent(U("branch")))
    {
        setBranch(ModelBase::stringFromHttpContent(multipart->getContent(U("branch"))));
                
    }
    if(multipart->hasContent(U("source")))
    {
        setSource(ModelBase::stringFromHttpContent(multipart->getContent(U("source"))));
                
    }
    if(multipart->hasContent(U("public_key")))
    {
        setPublicKey(ModelBase::stringFromHttpContent(multipart->getContent(U("public_key"))));
                
    }
    if(multipart->hasContent(U("fee")))
    {
        setFee(ModelBase::int32_tFromHttpContent(multipart->getContent(U("fee"))));
    }
    if(multipart->hasContent(U("counter")))
    {
        setCounter(ModelBase::int32_tFromHttpContent(multipart->getContent(U("counter"))));
    }
    {
        m_Operations.clear();
        if(multipart->hasContent(U("operations")))
        {            
        
        web::json::value jsonArray = web::json::value::parse(ModelBase::stringFromHttpContent(multipart->getContent(U("operations"))));
        for( auto& item : jsonArray.as_array() )
        {
            
            if(item.is_null()) 
            {
                m_Operations.push_back( std::shared_ptr<OriginationOperation>(nullptr) );
            }
            else
            {
                std::shared_ptr<OriginationOperation> newItem(new OriginationOperation());
                newItem->fromJson(item);
                m_Operations.push_back( newItem );
            }
            
        }
        }
    }
    if(multipart->hasContent(U("level")))
    {
        setLevel(ModelBase::int32_tFromHttpContent(multipart->getContent(U("level"))));
    }
    if(multipart->hasContent(U("block_hash")))
    {
        setBlockHash(ModelBase::stringFromHttpContent(multipart->getContent(U("block_hash"))));
                
    }
    if(multipart->hasContent(U("time")))
    {
        setTime(ModelBase::dateFromHttpContent(multipart->getContent(U("time"))));
                
    }
    
}
    
   
utility::string_t Origination::getHash() const
{
	return m_Hash;
}
void Origination::setHash(utility::string_t value)
{
	m_Hash = value;
    m_HashIsSet = true;
}
bool Origination::hashIsSet() const
{
    return m_HashIsSet;
}
void Origination::unsetHash() 
{
    m_HashIsSet = false;
}
utility::string_t Origination::getBranch() const
{
	return m_Branch;
}
void Origination::setBranch(utility::string_t value)
{
	m_Branch = value;
    m_BranchIsSet = true;
}
bool Origination::branchIsSet() const
{
    return m_BranchIsSet;
}
void Origination::unsetBranch() 
{
    m_BranchIsSet = false;
}
utility::string_t Origination::getSource() const
{
	return m_Source;
}
void Origination::setSource(utility::string_t value)
{
	m_Source = value;
    m_SourceIsSet = true;
}
bool Origination::sourceIsSet() const
{
    return m_SourceIsSet;
}
void Origination::unsetSource() 
{
    m_SourceIsSet = false;
}
utility::string_t Origination::getPublicKey() const
{
	return m_Public_key;
}
void Origination::setPublicKey(utility::string_t value)
{
	m_Public_key = value;
    m_Public_keyIsSet = true;
}
bool Origination::public_keyIsSet() const
{
    return m_Public_keyIsSet;
}
void Origination::unsetPublic_key() 
{
    m_Public_keyIsSet = false;
}
int32_t Origination::getFee() const
{
	return m_Fee;
}
void Origination::setFee(int32_t value)
{
	m_Fee = value;
    m_FeeIsSet = true;
}
bool Origination::feeIsSet() const
{
    return m_FeeIsSet;
}
void Origination::unsetFee() 
{
    m_FeeIsSet = false;
}
int32_t Origination::getCounter() const
{
	return m_Counter;
}
void Origination::setCounter(int32_t value)
{
	m_Counter = value;
    m_CounterIsSet = true;
}
bool Origination::counterIsSet() const
{
    return m_CounterIsSet;
}
void Origination::unsetCounter() 
{
    m_CounterIsSet = false;
}
std::vector<std::shared_ptr<OriginationOperation>>& Origination::getOperations()
{
	return m_Operations;
}
bool Origination::operationsIsSet() const
{
    return m_OperationsIsSet;
}
void Origination::unsetOperations() 
{
    m_OperationsIsSet = false;
}
int32_t Origination::getLevel() const
{
	return m_Level;
}
void Origination::setLevel(int32_t value)
{
	m_Level = value;
    m_LevelIsSet = true;
}
bool Origination::levelIsSet() const
{
    return m_LevelIsSet;
}
void Origination::unsetLevel() 
{
    m_LevelIsSet = false;
}
utility::string_t Origination::getBlockHash() const
{
	return m_Block_hash;
}
void Origination::setBlockHash(utility::string_t value)
{
	m_Block_hash = value;
    m_Block_hashIsSet = true;
}
bool Origination::block_hashIsSet() const
{
    return m_Block_hashIsSet;
}
void Origination::unsetBlock_hash() 
{
    m_Block_hashIsSet = false;
}
utility::datetime Origination::getTime() const
{
	return m_Time;
}
void Origination::setTime(utility::datetime value)
{
	m_Time = value;
    m_TimeIsSet = true;
}
bool Origination::timeIsSet() const
{
    return m_TimeIsSet;
}
void Origination::unsetTime() 
{
    m_TimeIsSet = false;
}

}
}
}
}

