/**
 * TezosAPI
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Candlestick.h
 * 
 * 
 */

#ifndef Candlestick_H_
#define Candlestick_H_


#include "ModelBase.h"

#include <cpprest/details/basic_types.h>

namespace info {
namespace tezex {
namespace client {
namespace model {

/// <summary>
/// 
/// </summary>
class  Candlestick
	: public ModelBase
{
public:
    Candlestick();
    virtual ~Candlestick();

	/////////////////////////////////////////////
	/// ModelBase overrides
	
    void validate() override;

    web::json::value toJson() const override;
    void fromJson(web::json::value& json) override;

    void toMultipart(std::shared_ptr<MultipartFormData> multipart, const utility::string_t& namePrefix) const override;
    void fromMultiPart(std::shared_ptr<MultipartFormData> multipart, const utility::string_t& namePrefix) override;
    
 	/////////////////////////////////////////////
	/// Candlestick members
	   
    /// <summary>
    /// 
    /// </summary>
    utility::string_t getO() const;
    void setO(utility::string_t value);
    bool oIsSet() const;
    void unseto();
    /// <summary>
    /// 
    /// </summary>
    utility::string_t getH() const;
    void setH(utility::string_t value);
    bool hIsSet() const;
    void unseth();
    /// <summary>
    /// 
    /// </summary>
    utility::string_t getL() const;
    void setL(utility::string_t value);
    bool lIsSet() const;
    void unsetl();
    /// <summary>
    /// 
    /// </summary>
    utility::string_t getC() const;
    void setC(utility::string_t value);
    bool cIsSet() const;
    void unsetc();
    /// <summary>
    /// 
    /// </summary>
    utility::string_t getVol() const;
    void setVol(utility::string_t value);
    bool volIsSet() const;
    void unsetVol();
    /// <summary>
    /// 
    /// </summary>
    utility::datetime getTime() const;
    void setTime(utility::datetime value);
    bool timeIsSet() const;
    void unsetTime();
    
protected:
    utility::string_t m_o;
    bool m_oIsSet;
utility::string_t m_h;
    bool m_hIsSet;
utility::string_t m_l;
    bool m_lIsSet;
utility::string_t m_c;
    bool m_cIsSet;
utility::string_t m_Vol;
    bool m_VolIsSet;
utility::datetime m_Time;
    bool m_TimeIsSet;
};

}
}
}
}

#endif /* Candlestick_H_ */
