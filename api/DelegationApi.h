/**
 * TezosAPI
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * DelegationApi.h
 * 
 * 
 */
 
#ifndef DelegationApi_H_
#define DelegationApi_H_


#include "ApiClient.h"

#include <cpprest/details/basic_types.h>
#include "Delegation.h"

namespace info {
namespace tezex {
namespace client {
namespace api {

using namespace info::tezex::client::model;

class  DelegationApi
{
public:
    DelegationApi( std::shared_ptr<ApiClient> apiClient );
    virtual ~DelegationApi();
    /// <summary>
    /// Get Delegation
    /// </summary>
    /// <remarks>
    /// Get a specific Delegation
    /// </remarks>
    /// <param name="delegationHash">The hash of the Origination to retrieve</param>
    pplx::task<std::shared_ptr<Delegation>> getDelegation(utility::string_t delegationHash);
    
protected:
    std::shared_ptr<ApiClient> m_ApiClient;    
};
    
}
}
}
}

#endif /* DelegationApi_H_ */

